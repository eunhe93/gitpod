# 4. intelli IDE

Date: 2023-02-13

## Status

Accepted

Supersedes [3. VSCode for IDE](0003-vscode-for-ide.md)

## Context

The issue motivating this decision, and any context that influences or constrains the decision.

## Decision

The change that we're proposing or have agreed to implement.

## Consequences

What becomes easier or more difficult to do and any risks introduced by the change that will need to be mitigated.
