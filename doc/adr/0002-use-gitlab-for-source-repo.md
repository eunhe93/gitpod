# 2. User Gitlab for Source Repo

Date: 2023-02-13

## Status

Accepted

## Context

GitHub is not available for on-premise.
Gitlab can be used in on-premise hosting.
Gitlab is open source.

## Decision

We decide Sorce Repo to Gitlab cloud.


## Consequences

We will use git cloud until MVP version.
Intra tean will setup on-premise gitlab server.
They will migrate cloud to on-premise.
