# 3. VSCode for IDE

Date: 2023-02-13

## Status

Superseded by [4. intelli IDE](0004-intelli-ide.md)

## Context

IDEd
- vscode : free,
- intelli J : commercial
- eclipse : free

vscode is most popular ide.

## Decision

We will user VSCode IDE.

## Consequences

QA team should make coding convention to vs setting file.
